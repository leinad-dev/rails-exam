class ExamsController < ApplicationController
  before_action :set_exam, only: %i[show edit update destroy]
  after_action :send_notifications, only: :create

  layout 'app'

  # GET /exams
  # GET /exams.json
  def index
    @exams = Exam.all
  end

  # GET /exams/1
  # GET /exams/1.json
  def show
  end

  # GET /exams/new
  def new
    @exam = Exam.new
  end

  # GET /exams/1/edit
  def edit
  end

  # POST /exams
  # POST /exams.json
  def create
    @exam = Exam.new(exam_params)

    @exam_type = ExamType.find(params[:exam][:exam_type_id])
    if @exam_type.nil?
      render :new
    else
      @exam.exam_type = @exam_type
    end

    respond_to do |format|
      if @exam.valid?
        @exam.slug = exam_slug
        @exam.save

        SendSmsJob.perform_later @exam

        format.html {redirect_to @exam, notice: 'Exam was successfully created.'}
        format.json {render :show, status: :created, location: @exam}
      else
        format.html {render :new}
        format.json {render json: @exam.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /exams/1
  # PATCH/PUT /exams/1.json
  def update
    @exam_type = ExamType.find(params[:exam][:exam_type_id])
    if @exam_type.nil?
      render :new
    else
      @exam.exam_type = @exam_type
    end

    respond_to do |format|
      if @exam.update(exam_params)
        attributes = exam_params
        attributes[:slug] = exam_slug
        @exam.update(attributes)

        format.html {redirect_to @exam, notice: 'Exam was successfully updated.'}
        format.json {render :show, status: :ok, location: @exam}
      else
        format.html {render :edit}
        format.json {render json: @exam.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /exams/1
  # DELETE /exams/1.json
  def destroy
    @exam.destroy
    respond_to do |format|
      format.html {redirect_to exams_url, notice: 'Exam was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_exam
    @exam = Exam.find_by_slug(params[:slug])
  end

  def exam_slug
    "#{@exam_type.label} #{params[:exam][:year]} #{DateTime.current.to_i}".parameterize
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def exam_params
    params.require(:exam).permit(:year, :price, :open_at, :is_current, :activated)
  end

  def send_notifications
    SendSmsWorker.perform_async @exam.id

    ExamMailer.exam_registered(@exam.id).deliver_later
  end
end
