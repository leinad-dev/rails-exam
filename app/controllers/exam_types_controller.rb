class ExamTypesController < ApplicationController
  before_action :set_exam_type, only: [:show, :edit, :update, :destroy]

  layout 'app'

  # GET /exam_types
  # GET /exam_types.json
  def index
    @exam_types = ExamType.all
  end

  # GET /exam_types/1
  # GET /exam_types/1.json
  def show
  end

  # GET /exam_types/new
  def new
    @exam_type = ExamType.new
  end

  # GET /exam_types/1/edit
  def edit
  end

  # POST /exam_types
  # POST /exam_types.json
  def create
    @exam_type = ExamType.new(exam_type_params)

    respond_to do |format|
      if @exam_type.valid?
        exam_type_with_highest_code = ExamType.order(code: :desc).first
        @exam_type.code = exam_type_with_highest_code.nil? ? 0 : (exam_type_with_highest_code.code.to_i + 1)
        @exam_type.slug = exam_type_slug
        @exam_type.save

        format.html {redirect_to @exam_type, notice: 'Exam type was successfully created.'}
        format.json {render :show, status: :created, location: @exam_type}
      else
        format.html {render :new}
        format.json {render json: @exam_type.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /exam_types/1
  # PATCH/PUT /exam_types/1.json
  def update
    respond_to do |format|
      if @exam_type.valid?
        attributes = exam_type_params
        attributes[:slug] = exam_type_slug
        @exam_type.update(attributes)

        format.html {redirect_to @exam_type, notice: 'Exam type was successfully updated.'}
        format.json {render :show, status: :ok, location: @exam_type}
      else
        format.html {render :edit}
        format.json {render json: @exam_type.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /exam_types/1
  # DELETE /exam_types/1.json
  def destroy
    @exam_type.destroy
    respond_to do |format|
      format.html {redirect_to exam_types_url, notice: 'Exam type was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_exam_type
    @exam_type = ExamType.find_by_slug(params[:slug])
  end

  # Return Exam Type slug.
  def exam_type_slug
    "#{params[:exam_type][:label]} #{DateTime.current.to_i}".parameterize
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def exam_type_params
    params.require(:exam_type).permit(:label, :price)
  end
end
