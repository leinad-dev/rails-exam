class SendSmsWorker
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def perform(exam_id)
    exam = Exam.find exam_id
    @form = 'Rails-Exam'
    @to = '22891071570'
    @text = "Hello ! New exam #{exam.exam_type.label} #{exam.year} has been registred and will open #{exam.open_at}. Thanks for subscribing !"
    @access_token = '8TVY0ZXsH2kTbH1SBPU346jZF7idUGn9'

    response = HTTParty.get 'http://api.mobicomtogo.pro/api/smsinfo', query: {from: @form, to: @to, text: @text, 'access-token': @access_token}

    logger.info "Send SmS Job# fom: #{@form} to: #{@to} text: #{@text} => HTTParty Response: #{response.parsed_response}"
  end
end
