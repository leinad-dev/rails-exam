class Exam < ApplicationRecord
  validates :year, presence: true, uniqueness: {scope: :exam_type_id}
  validates :price, allow_nil: true, numericality: {only_integer: true}

  belongs_to :exam_type

  def to_param
    slug
  end

  def price
    self[:price].nil? ? exam_type.price : self[:price] unless new_record?
  end

  def open_at
    self[:open_at].strftime("%d-%m-%Y at %H:%M")
  end

end
