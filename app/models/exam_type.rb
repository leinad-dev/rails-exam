class ExamType < ApplicationRecord
  validates :label, presence: true, uniqueness: true
  validates :price, presence: true, numericality: {only_integer: true}

  has_many :exams

  def to_param
    slug
  end
end
