class ApplicationMailer < ActionMailer::Base
  default from: 'suisco@suisco.net'
  layout 'mailer'
end
