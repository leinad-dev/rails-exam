class ExamMailer < ApplicationMailer
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def exam_registered(exam_id)
    @exam = Exam.find exam_id

    mail to: "blandedaniel@yahoo.fr", subject: "Exam New Letter"
  end
end
