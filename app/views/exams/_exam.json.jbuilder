json.extract! exam, :id, :year, :price, :open_at, :is_current, :activated, :exam_type_id, :created_at, :updated_at
json.url exam_url(exam, format: :json)
