json.extract! exam_type, :id, :label, :code, :price, :slug, :created_at, :updated_at
json.url exam_type_url(exam_type, format: :json)
