Rails.application.routes.draw do
  resources :exams, param: :slug
  resources :exam_types, param: :slug

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  root 'exam_types#index'
end
