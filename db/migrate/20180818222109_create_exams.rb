class CreateExams < ActiveRecord::Migration[5.2]
  def change
    create_table :exams do |t|
      t.integer :year, null: false
      t.integer :price
      t.datetime :open_at, null: false
      t.boolean :is_current, default: false
      t.boolean :activated, default: false
      t.integer :pending_request_count, default: 0
      t.integer :pending_request_handle_count, default: 0
      t.boolean :must_be_dispatched, default: false
      t.boolean :is_dispatching, default: false
      t.datetime :dispatched_at
      t.string :job_uuid
      t.string :slug, index: true, unique: true
      t.references :exam_type, foreign_key: true

      t.timestamps
    end
  end
end
