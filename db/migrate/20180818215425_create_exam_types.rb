class CreateExamTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :exam_types do |t|
      t.string :label, unique: true, null: false
      t.string :code, unique: true, null: false
      t.integer :price, default: 0, null: false
      t.string :slug, index: true, unique: true

      t.timestamps
    end
  end
end
