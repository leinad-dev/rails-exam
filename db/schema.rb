# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_18_222109) do

  create_table "exam_types", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "label", null: false
    t.string "code", null: false
    t.integer "price", default: 0, null: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_exam_types_on_slug"
  end

  create_table "exams", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "price"
    t.datetime "open_at", null: false
    t.boolean "is_current", default: false
    t.boolean "activated", default: false
    t.integer "pending_request_count", default: 0
    t.integer "pending_request_handle_count", default: 0
    t.boolean "must_be_dispatched", default: false
    t.boolean "is_dispatching", default: false
    t.datetime "dispatched_at"
    t.string "job_uuid"
    t.string "slug"
    t.bigint "exam_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exam_type_id"], name: "index_exams_on_exam_type_id"
    t.index ["slug"], name: "index_exams_on_slug"
  end

  add_foreign_key "exams", "exam_types"
end
